const jwt = require("jsonwebtoken");
const Member = require("../models/members");

const auth = async (req, res, next) => {
	try{
		//1 access header
		//2 get jwt out of the header
		const token = req.header("Authorization").replace("Bearer ", "")

		//3 make sure that token is valid
		const decoded = jwt.verify(token, "batch40")
		//4 find the member in db
		//5 check if token is part of member.tokens
		const member = await Member.findOne({_id: decoded._id, "tokens.token": token})
		//6 check if member doesn't exist
		if(!member){
			throw new Error("Member doesn't exist")
		}
		//7 give route access to member
		req.member = member
		//8 give route access to token
		req.token = token
		//9 call next function
		next();
	}catch(e){
		res.status(401).send({"message":"Please authenticate"});
	}

}

module.exports = auth;