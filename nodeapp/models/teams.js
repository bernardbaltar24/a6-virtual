//Declare Dependencies

const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const validator = require("validator");

//Define your schema

const teamSchema = new Schema(
	{
		name:{
			type: String,
			required: true,
			lowercase: true,
			trim: true
		},
		budget:
		{
			type:Number,
			required:[true, "Budget is required!"],
			validate(value){
				if(value < 100){
				throw new Error("Budget must be more than 100!") }
			}
		},
		level:
		{
			type: Number,
			enum: [1, 2, 3],
			default: 1,	
		},
		email:
		{
			type: String,
			required: true,
			validate(value){
				if(!validator.isEmail(value)){
					throw new Error("Email is invalid!")
				}
				//SANITATION
				return (this.email = validator.normalizeEmail(value, {all_lowercase: true})) //>>>TO SANITISE ALL EMAILS WILL BE IN LOWER CASE IN DATABASE
			}
		},

		isActive: {
			type: Boolean,
			required: true,
			validate(value){
				if(!validator.isBoolean){
					return (this.isActive = validator.toBoolean(value))
				}
			}
		}
	},
	{
		timestamps: true
	}
	);

//SET THE RELATIONSHIP BETWEEN TEAM AND MEMBER
teamSchema.virtual("members", {
	ref: "Member",
	localField: "_id", //team ID
	foreignField: "teamId" //foreign key ng member n teamId

})

//Export your model

module.exports = mongoose.model("Team", teamSchema);