//Declare dependencies and model
const Task = require("../models/tasks");
const express = require("express");
const router = express.Router(); //to handle routing
const auth = require("../middleware/auth");

// --------------TASKS
// 1) CREATE TASK 
router.post("/", auth, async (req, res) =>{
	const task = new Task({
		//use Spread Operator to copy the body
		...req.body,
		memberId: req.member._id
	});
	// task.save().then(() => {res.send(task)})
	// .catch((e) => {res.status(400).send(e)})
	try{
		await task.save();
		res.status(201).send(task);
	}catch(e){
		res.status(400).send(e)
	}
})
//2 GET ALL TASKS OF LOGGED IN MEMBER

router.get("/", auth, async (req, res)=>{

	// console.log(req.query)
	const match = {};
	if(req.query.isCompleted){
		match.isCompleted = req.query.isCompleted
	}


	// Task.find().then((tasks) => { return res.status(200).send(tasks)})
	// .catch((e) => { return res.status(500).send(e)})
	try {
		// const tasks = await Task.find({memberId: req.member._id}); //basic daw
		await req.member.populate({
			path: "tasks",
			// match: req.query
			match //match:match
		}).execPopulate();
		res.status(200).send(req.member.tasks)
	} catch(e){
		res.status(500).send(e)
	}
})
//3)GET ONE TASK
router.get("/:id", auth, async (req, res) =>{
	const _id = req.params.id;
	// Task.findById(_id).then((task) => {if(!task){
	// 	return res.status(404).send(e)
	// } return res.send(task)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const task = await Task.findById(_id);
		if(!task){
		return res.status(404).send("WALANG MAHANAP NA TASK")};
		res.send(task)
	}catch(e){
		res.status(500).send(e)
	}
})
//4)UPDATE ONE TASK
router.patch("/:id", auth, async (req, res) =>{
	const _id = req.params.id
	// Task.findByIdAndUpdate(_id, req.body, {new:true}).then((task) => {
	// 	if(!task){return res.status(404).send(e)}
	// 	return res.send(task)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const task = await Task.findByIdAndUpdate(_id, req.body, { new:true });
		if(!task){
		return res.status(404).send("WALANG MAHANAP")};
		res.send(task)
	} catch(e){
		return res.status(500).send(e)
		}
})
//5)DELETE ONE TASK
router.delete("/:id", auth, async (req, res)=> {
	const _id = req.params.id;
	// Task.findByIdAndDelete(_id).then((task) => {if(!task){return res.status(404).send(e)}
	// 	return res.send(task)
	// })
	// .catch((e) => {return res.status(500).send(e)})
	try{
		const task = await Task.findByIdAndDelete(_id);
		if(!task){
			return res.status(404).send("Task doesn't exist or whatever")
		}	res.send(task)
	}catch(e){
		res.status(500).send(e.message)
	}
})

module.exports = router;